﻿import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Change Container Color'),
        ),
        body: ContainerColorChanger(),
      ),
    );
  }
}

class ContainerColorChanger extends StatefulWidget {
  @override
  _ContainerColorChangerState createState() => _ContainerColorChangerState();
}

class _ContainerColorChangerState extends State<ContainerColorChanger> {
  Color _containerColor = Colors.blue; // Màu mặc định của container

  void _changeColor() {
    setState(() {
      // Đổi màu của container sang màu ngẫu nhiên
      _containerColor =
          Colors.primaries[Random().nextInt(Colors.primaries.length)];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 200,
          height: 200,
          color: _containerColor, // Màu của container
          child: Center(
            child: Text(
              'Container',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
        ),
        SizedBox(height: 20),
        ElevatedButton(
          onPressed:
              _changeColor, // Khi bấm nút, gọi hàm để thay đổi màu container
          child: Text('Change Color'),
        ),
      ],
    );
  }
}
