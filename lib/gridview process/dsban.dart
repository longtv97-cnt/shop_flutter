﻿import 'dart:math';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: AdminApp(),
    debugShowCheckedModeBanner: false,
  ));
}

class Ban {
  final String maBan;
  final String tenBan;
  final int trangThai; // 0: trống, 1: có khách

  Ban({required this.maBan, required this.tenBan, required this.trangThai});
}

class AdminApp extends StatelessWidget {
  final List<Ban> danhSachBan = List.generate(
    30,
    (index) => Ban(
      maBan: (index + 1).toString(),
      tenBan: 'Table ${index + 1}',
      trangThai: Random().nextInt(2), // Random trạng thái 0 hoặc 1
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Danh sách bàn'),
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 5,
          crossAxisSpacing: 4.0,
          mainAxisSpacing: 4.0,
        ),
        itemCount: danhSachBan.length,
        itemBuilder: (context, index) {
          return _buildBanContainer(danhSachBan[index]);
        },
      ),
    );
  }

  Widget _buildBanContainer(Ban ban) {
    Color backgroundColor = ban.trangThai == 1 ? Colors.red : Colors.green;
    String trangThaiText = ban.trangThai == 1 ? 'Có khách' : 'Trống';

    return Container(
      color: backgroundColor,
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                ban.tenBan,
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            ],
          ),
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 4, vertical: 2),
              color: Colors.white,
              child: Text(
                trangThaiText,
                style: TextStyle(color: backgroundColor, fontSize: 12),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
