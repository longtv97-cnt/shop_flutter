﻿import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: MyGridView(),
      ),
    );
  }
}

class MyGridView extends StatefulWidget {
  @override
  _MyGridViewState createState() => _MyGridViewState();
}

class _MyGridViewState extends State<MyGridView> {
  GlobalKey gridKey = GlobalKey(); // GlobalKey để truy cập GridView

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      key: gridKey, // Truyền GlobalKey vào GridView
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 4.0,
        mainAxisSpacing: 4.0,
        childAspectRatio: 1.0,
      ),
      itemCount: 12,
      itemBuilder: (BuildContext context, int index) {
        String itemText = 'Item $index';
        Color backgroundColor = itemText == 'Item 12' ? Colors.red : Colors.blue;
        return GestureDetector(
          onTap: () {
            if (backgroundColor == Colors.blue) {
              // Nếu nền là màu xanh, chuyển đến trang đặt món
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => OrderPage()),
              );
            }
          },
          child: Container(
            width: 50,
            height: 50,
            color: backgroundColor,
            child: Center(
              child: Text(
                itemText,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        );
      },
    );
  }
}

class OrderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order Page'),
      ),
      body: Center(
        child: Text('This is the order page.'),
      ),
    );
  }
}
