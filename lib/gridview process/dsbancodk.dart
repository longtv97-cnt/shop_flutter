﻿import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(MaterialApp(
    home: AdminApp(),
    debugShowCheckedModeBanner: false,
  ));
}

class AdminApp extends StatelessWidget {
  late final List<Ban> danhSachBan = generateDanhSachBan();

  List<Ban> generateDanhSachBan() {
    List<Ban> danhSachBan = [];

    for (int i = 0; i < 20; i++) {
      danhSachBan.add(Ban(
        maBan: (i + 1).toString(),
        tenBan: 'Table ${i + 1}',
        trangThai: 0, // Bàn trống
      ));
    }

    for (int i = 20; i < 30; i++) {
      danhSachBan.add(Ban(
        maBan: (i + 1).toString(),
        tenBan: 'Table ${i + 1}',
        trangThai: 1, // Bàn có khách
      ));
    }
    danhSachBan.shuffle();
    return danhSachBan;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Danh sách bàn'),
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 5,
          crossAxisSpacing: 4.0,
          mainAxisSpacing: 4.0,
        ),
        itemCount: danhSachBan.length,
        itemBuilder: (context, index) {
          return _buildBanContainer(context, danhSachBan[index]);
        },
      ),
    );
  }

  Widget _buildBanContainer(BuildContext context, Ban ban) {
    Color backgroundColor = ban.trangThai == 1 ? Colors.red : Colors.green;
    String trangThaiText = ban.trangThai == 1 ? 'Có khách' : 'Trống';

    return GestureDetector(
      onTap: () {
        if (ban.trangThai == 0) {
          // Nếu bàn trống (màu xanh), chuyển đến trang đặt món
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => OrderPage(ban: ban)),
          );
        }
      },
      child: Container(
        color: backgroundColor,
        child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Text(
                    ban.tenBan,
                    style: TextStyle(color: Colors.white, fontSize: 14,),
                  ),
                ),
              ],
            ),
            Positioned(
              top: 0,
              left: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                color: Colors.white,
                child: Text(
                  trangThaiText,
                  style: TextStyle(color: backgroundColor, fontSize: 12),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Ban {
  String maBan;
  String tenBan;
  int trangThai; // 0: Bàn trống, 1: Bàn có khách

  Ban({required this.maBan, required this.tenBan, required this.trangThai});
}

class OrderPage extends StatelessWidget {
  final Ban ban;

  OrderPage({required this.ban});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order for ${ban.tenBan}'),
      ),
      body: Center(
        child: Text('Order page for ${ban.tenBan}'),
      ),
    );
  }
}
