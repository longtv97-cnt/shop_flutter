﻿import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: AdminApp(),
    debugShowCheckedModeBanner: false,
  ));
}

class AdminApp extends StatefulWidget {
  @override
  _AdminAppState createState() => _AdminAppState();
}

class _AdminAppState extends State<AdminApp> {
  List<String> tables = List.generate(30, (index) => 'Table ${index + 1}');
  List<String> freeTables = [];
  List<String> reservedTables = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tables'),
      ),
      body: DefaultTabController(
        length: 3,
        child: Column(
          children: [
            TabBar(
              tabs: [
                Tab(text: 'All'),
                Tab(text: 'Free'),
                Tab(text: 'Reserved'),
              ],
            ),
            Expanded(
              child: TabBarView(
                children: [
                  // All Tab View
                  GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 4.0,
                      mainAxisSpacing: 4.0,
                    ),
                    itemCount: tables.length,
                    itemBuilder: (context, index) {
                      return _buildTableContainer(tables[index]);
                    },
                  ),

                  // Free Tab View
                  GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 4.0,
                      mainAxisSpacing: 4.0,
                    ),
                    itemCount: freeTables.length,
                    itemBuilder: (context, index) {
                      return _buildTableContainer(freeTables[index], isReserved: false);
                    },
                  ),

                  // Reserved Tab View
                  GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 4.0,
                      mainAxisSpacing: 4.0,
                    ),
                    itemCount: reservedTables.length,
                    itemBuilder: (context, index) {
                      return _buildTableContainer(reservedTables[index], isReserved: true);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTableContainer(String table, {bool isReserved = false}) {
    bool isOddTable = int.parse(table.split(' ')[1]) % 2 != 0;

    if (isOddTable) {
      return GestureDetector(
        onTap: () {
          // Thực hiện chuyển đến trang thanh toán ở đây
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => PaymentPage()),
          );
        },
        child: Container(
          color: Colors.red, // Màu đỏ cho bàn số lẻ
          child: Center(
            child: Text(
              '$table\n${isReserved ? 'Occupied' : 'Empty'}',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      );
    } else {
      // Container cho bàn số chẵn
      Color color = isReserved ? Colors.red : Colors.green;
      String status = isReserved ? 'Occupied' : 'Empty';
      return GestureDetector(
        onTap: () {
          // Thực hiện chuyển đến trang đặt món ở đây
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => OrderingPage()),
          );
        },
        child: Container(
          color: color,
          child: Center(
            child: Text(
              '$table\n$status',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      );
    }
  }
}

class PaymentPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Payment'),
      ),
      body: Center(
        child: Text('Payment Page'),
      ),
    );
  }
}

class OrderingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ordering'),
      ),
      body: Center(
        child: Text('Ordering Page'),
      ),
    );
  }
}
