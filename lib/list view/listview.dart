﻿import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: StationList(),
    );
  }
}

class StationList extends StatelessWidget {
  StationList({Key? key}) : super(key: key);

  List<Station> stations = List.generate(
    10,
    (index) => Station(
      id: index,
      name: 'Station ${index + 1}',
      type: Random().nextInt(2) == 0 ? 'private' : 'public',
      status: Random().nextBool(),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('List of Stations'),
      ),
      body: Column(
        children: [
          const Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children: [
              Text(
                'Status',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(width: 20),
              Text(
                'Name',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(width: 20),
              Text(
                'Type',
                style: TextStyle(fontSize: 20),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: stations.length,
              itemBuilder: (context, index) {
                return ListTile(
                  leading: stations[index].status
                      ? const Icon(Icons.online_prediction, color: Colors.green)
                      : const Icon(Icons.offline_bolt, color: Colors.red),
                  title: Row(
                    children: [
                      SizedBox.square(
                        dimension: MediaQuery.of(context).size.width * 0.34,
                      ),
                      Text(stations[index].name),
                    ],
                  ),
                  trailing: stations[index].type == 'private'
                      ? const Icon(Icons.lock, color: Colors.red)
                      : const Icon(Icons.public, color: Colors.blue),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class Station {
  int id;
  String name;
  String type;
  bool status;
  Station({
    required this.id,
    required this.name,
    required this.type,
    required this.status,
  });
}
