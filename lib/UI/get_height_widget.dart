﻿import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Get Height Example'),
        ),
        body: Center(
          child: Container(
            height: 100, // Đây là chiều cao của đối tượng Container
            color: Colors.blue,
            child: ElevatedButton(
              onPressed: () {
                // In ra chiều cao của đối tượng Container khi nút được nhấn
                print('Chiều cao của Container là: ${context.size.toString()}');
                print('Chiều cao của thiết bị  là: ${mediaQuery.size.height}');
              },
              child: Text('In Chiều Cao'),
            ),
          ),
        ),
      ),
    );
  }
}
