﻿import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

void main() {
  runApp(const MaterialApp(
    home: MyWidget(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyWidget extends StatefulWidget {
  const MyWidget({Key? key}) : super(key: key);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  final List<int> _items = List<int>.generate(30, (int index) => index);

  @override
  Widget build(BuildContext context) {
    Offset initialOffset = const Offset(200, 250);

    return Scaffold(
      appBar: AppBar(
        title: const Text('ReorderableListView'),
      ),
      body: Center(
        child: Column(
          children: [
            // circle avatar
            CircleAvatar(
              radius: 100,
              child: const Text('A', style: TextStyle(fontSize: 50)),
              backgroundColor: Colors.green,
            ),
            CupertinoContextMenu(
              child: Container(
                height: 100,
                width: 100,
                color: Colors.blue,
              ),
              actions: <Widget>[
                CupertinoContextMenuAction(
                  child: const Text('Action one'),
                  onPressed: () {
                    print('Action one');
                  },
                ),
                CupertinoContextMenuAction(
                  child: const Text('Action two'),
                  onPressed: () {
                    // Navigator.push(context, MaterialPageRoute(builder: (context) => MyWidget()));
                    launchUrlString("https:www.google.com");
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
