﻿import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


void main() {
  runApp(const MaterialApp(
    home: MyWidget(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyWidget extends StatefulWidget {
  const MyWidget({Key? key}) : super(key: key);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  final List<int> _items = List<int>.generate(30, (int index) => index);

  @override
  Widget build(BuildContext context) {
    Offset initialOffset = const Offset(200, 250);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Alert Dialog'),
      ),
      body: Center(
        child: Column(
          children: [
            // circle avatar
            TextButton(
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return Theme(
                          data: ThemeData.dark().copyWith(
                            colorScheme: ColorScheme.dark(
                              primary: Colors.red,
                              onPrimary: Colors.white,
                              surface: Colors.green,
                              onSurface: Colors.yellow,
                            ),
                            dialogBackgroundColor: Colors.blue,
                          ),
                          child: AlertDialog(
                            title: Text('Alert Dialog'),
                            content: Text('This is an alert dialog'),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text('OK'))
                            ],
                          ));
                    },
                  );
                },
                child: Text('Show Alert Dialog'))
          ],
        ),
      ),
    );
  }
}
