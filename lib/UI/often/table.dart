﻿import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

void main() {
  runApp(const MaterialApp(
    home: MyWidget(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyWidget extends StatefulWidget {
  const MyWidget({Key? key}) : super(key: key);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  final List<int> _items = List<int>.generate(30, (int index) => index);

  @override
  Widget build(BuildContext context) {
    Offset initialOffset = const Offset(200, 250);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Table'),
      ),
      body: Center(
        child: Column(
          children: [
            // circle avatar
            Table(
              border: TableBorder.all(),
              children: [
                TableRow(
                  children: [
                    TableCell(
                      child: Container(
                        color: Colors.green,
                        padding: const EdgeInsets.all(8.0),
                        child: const Text('Cell 1'),
                      ),
                    ),
                    TableCell(
                      child: Container(
                        color: Colors.blue,
                        padding: const EdgeInsets.all(8.0),
                        child: const Text('Cell 2'),
                      ),
                    ),
                    TableCell(
                      child: Container(
                        color: Colors.red,
                        padding: const EdgeInsets.all(8.0),
                        child: const Text('Cell 3'),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Container(
                        color: Colors.yellow,
                        padding: const EdgeInsets.all(8.0),
                        child: const Text('Cell 4'),
                      ),
                    ),
                    TableCell(
                      child: Container(
                        color: Colors.purple,
                        padding: const EdgeInsets.all(8.0),
                        child: const Text('Cell 5'),
                      ),
                    ),
                    TableCell(
                      child: Container(
                        color: Colors.orange,
                        padding: const EdgeInsets.all(8.0),
                        child: const Text('Cell 6'),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
