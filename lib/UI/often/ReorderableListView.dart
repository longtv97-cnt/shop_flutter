﻿import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(const MaterialApp(
    home: MyWidget(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyWidget extends StatefulWidget {
  const MyWidget({Key? key}) : super(key: key);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  final List<int> _items = List<int>.generate(30, (int index) => index);

  @override
  Widget build(BuildContext context) {
    Offset initialOffset = const Offset(200, 250);

    return Scaffold(
      appBar: AppBar(
        title: const Text('ReorderableListView'),
      ),
      body: Center(
        child: Column(
          children: [
            // circle avatar
            CircleAvatar(
              radius: 30,
              child: const Text('A'),
              backgroundColor: Colors.green,
            ),
            Expanded(
              child: ReorderableListView(
                children: _items
                    .map((int index) => ListTile(
                          key: ValueKey<int>(index),
                          title: Text('Item ${index + 1}'),
                        ))
                    .toList(),
                onReorder: (int oldIndex, int newIndex) {
                  setState(() {
                    if (oldIndex < newIndex) {
                      newIndex -= 1;
                    }
                    final int item = _items.removeAt(oldIndex);
                    _items.insert(newIndex, item);
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
