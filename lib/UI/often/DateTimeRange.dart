﻿import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    home: MyWidget(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyWidget extends StatefulWidget {
  const MyWidget({Key? key}) : super(key: key);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  DateTimeRange? _dateTimeRange =
      DateTimeRange(start: DateTime.now(), end: DateTime.now());

  @override
  Widget build(BuildContext context) {
    Offset initialOffset = const Offset(200, 250);

    return Scaffold(
      appBar: AppBar(
        title: const Text('DateTimeRange'),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          Text('Chọn thời gian : ${_dateTimeRange?.duration.inDays} ngày'),
          ElevatedButton(
              onPressed: () async {
                final DateTimeRange? picked = await showDateRangePicker(
                  context: context,
                  firstDate: DateTime(DateTime.now().year - 1),
                  lastDate: DateTime(DateTime.now().year + 1),
                  initialDateRange: _dateTimeRange,
                  confirmText: "Chọn",
                  cancelText: "Hủy",
                  saveText: "Lưu",
                  fieldStartHintText: "Ngày bắt đầu",
                  helpText: "Select a date range",
                  errorFormatText: "Loi dinh dang",
                  errorInvalidText: "Loi khong hop le",
                  fieldEndHintText: "Ngay ket thuc",
                  fieldStartLabelText: "Bat dau",
                  fieldEndLabelText: "Ket thuc",
                  builder: (BuildContext context, Widget? child) {
                    return Theme(
                      data: ThemeData.light().copyWith(
                        colorScheme: ColorScheme.light(
                          primary: Colors.black,
                          onPrimary: Colors.white54,
                          surface: Colors.redAccent,
                          onSurface: Colors.black,
                        ),
                        dialogBackgroundColor: Colors.purple,
                      ),
                      child: child!,
                    );
                  },
                );
                if (picked != null && picked != _dateTimeRange) {
                  setState(() {
                    _dateTimeRange = picked;
                  });
                }
              },
              child: const Text('Show DateTimeRange')),
        ],
      )),
    );
  }
}
