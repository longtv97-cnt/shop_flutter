﻿import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    home: MyWidget(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyWidget extends StatefulWidget {
  const MyWidget({Key? key}) : super(key: key);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  static List<String> _fruits = [
    'Apple',
    'Banana',
    'Mango',
    'Orange',
    'Pineapple',
    'Strawberry'
  ];
  var fController = TextEditingController();
  var filteredFruits = <String>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          TextField(
            controller: fController,
            decoration: InputDecoration(
              hintText: 'Enter a fruit',
            ),
            onChanged: (String value) {
              setState(() {
                filteredFruits = _fruits
                    .where((element) =>
                        element.toLowerCase().contains(value.toLowerCase()))
                    .toList();
              });
            },
          ),
          Expanded(
            child: ListView.builder(
              itemCount: filteredFruits.isNotEmpty
                  ? filteredFruits.length
                  : _fruits.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(filteredFruits.isNotEmpty
                      ? filteredFruits[index]
                      : _fruits[index]),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    fController.dispose();
    super.dispose();
  }
}
