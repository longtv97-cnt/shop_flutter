﻿import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _textEditingController = TextEditingController();
  bool _showCustomKeyboard = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Custom Keyboard'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            controller: _textEditingController,
            decoration: InputDecoration(
              hintText: 'Enter text...',
              contentPadding: EdgeInsets.all(10.0),
              border: OutlineInputBorder(),
            ),
          ),
          _showCustomKeyboard
              ? CustomKeyboard(
                  onTextInput: (key) {
                    _textEditingController.text += key;
                  },
                  onBackspace: () {
                    if (_textEditingController.text.isNotEmpty) {
                      setState(() {
                        _textEditingController.text =
                            _textEditingController.text.substring(
                                0, _textEditingController.text.length - 1);
                      });
                    }
                  },
                  onDone: () {
                    setState(() {
                      _showCustomKeyboard = false;
                    });
                  },
                )
              : SizedBox(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _showCustomKeyboard = !_showCustomKeyboard;
          });
        },
        child: Icon(_showCustomKeyboard ? Icons.keyboard_hide : Icons.keyboard),
      ),
    );
  }
}

class CustomKeyboard extends StatelessWidget {
  final Function(String) onTextInput;
  final Function() onBackspace;
  final Function() onDone;

  CustomKeyboard({
    required this.onTextInput,
    required this.onBackspace,
    required this.onDone,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      color: Colors.grey[200],
      child: Column(
        children: [
          buildRow(['1', '2', '3']),
          buildRow(['4', '5', '6']),
          buildRow(['7', '8', '9']),
          buildRow(['Mode', '0', '⌫']),
          buildRow(['Done']),
        ],
      ),
    );
  }

  Widget buildRow(List<String> keys) {
    return Expanded(
      child: Row(
        children: keys
            .map(
              (key) => Expanded(
                child: InkWell(
                  onTap: () {
                    if (key == '⌫') {
                      onBackspace();
                    } else if (key == 'Done') {
                      onDone();
                    } else {
                      onTextInput(key);
                    }
                  },
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Text(
                      key,
                      style: TextStyle(fontSize: 24),
                    ),
                  ),
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
