﻿import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Custom Keyboard',
      home: KeyboardDemo(),
    );
  }
}

class KeyboardDemo extends StatefulWidget {
  @override
  _KeyboardDemoState createState() => _KeyboardDemoState();
}

class _KeyboardDemoState extends State<KeyboardDemo> {
  TextEditingController _textController = TextEditingController();

  bool isNumericKeyboard = false;

  void toggleKeyboardMode() {
    setState(() {
      isNumericKeyboard = !isNumericKeyboard;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Custom Keyboard Demo'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TextField(
            controller: _textController,
            keyboardType:
                isNumericKeyboard ? TextInputType.number : TextInputType.text,
            decoration: InputDecoration(
              labelText: 'Enter your text',
            ),
          ),
          SizedBox(height: 16),
          buildKeyboard(),
          SizedBox(height: 16),
          ElevatedButton(
            onPressed: toggleKeyboardMode,
            child: Text(isNumericKeyboard ? 'ABC' : '?123'),
          ),
        ],
      ),
    );
  }

  Widget buildKeyboard() {
    return Container(
      padding: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        color: Colors.grey[300],
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Column(
        children: <Widget>[
          if (!isNumericKeyboard) ...[
            buildRow(['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P']),
            buildRow(['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L']),
            buildRow(['Z', 'X', 'C', 'V', 'B', 'N', 'M']),
          ],
          if (isNumericKeyboard) ...[
            buildRow(['1', '2', '3']),
            buildRow(['4', '5', '6']),
            buildRow(['7', '8', '9']),
            buildRow(['C', '0', '=']),
          ],
        ],
      ),
    );
  }

  Widget buildRow(List<String> keys) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: keys.map((key) {
        return Expanded(
          child: ElevatedButton(
            onPressed: () {
              if (key == 'C') {
                _textController.clear();
              } else if (key == '=') {
                calculateResult();
              } else if (key == '<-') {
                if (_textController.text.isNotEmpty) {
                  _textController.text = _textController.text
                      .substring(0, _textController.text.length - 1);
                }
              } else {
                _textController.text += key;
              }
            },
            child: Text(key),
          ),
        );
      }).toList(),
    );
  }

  void calculateResult() {
    try {
      // Thực hiện tính toán từ chuỗi trong TextField
      String expression = _textController.text;
      // Ví dụ đơn giản:
      // - Sử dụng package math_expressions để tính toán biểu thức
      // - Trong trường hợp này, để đơn giản, sẽ chỉ tính toán các biểu thức cơ bản
      double result = evaluateExpression(expression);
      // Hiển thị kết quả
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Result'),
          content: Text('Result: $result'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('OK'),
            ),
          ],
        ),
      );
    } catch (e) {
      // Xử lý nếu có lỗi xảy ra trong quá trình tính toán
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Error'),
          content: Text('Invalid expression!'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  double evaluateExpression(String expression) {
    // Đây chỉ là ví dụ đơn giản, bạn có thể sử dụng các công cụ mạnh mẽ hơn để tính toán biểu thức
    return double.parse(expression);
  }
}
