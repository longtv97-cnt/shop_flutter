﻿import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Number Keyboard'),
        ),
        body: const NumberKeyboard(),
      ),
    );
  }
}

class NumberKeyboard extends StatelessWidget {
  const NumberKeyboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 8.0,
        crossAxisSpacing: 8.0,
      ),
      padding: const EdgeInsets.all(8.0),
      itemCount: 10, // Số lượng phím trong bàn phím
      itemBuilder: (BuildContext context, int index) {
        // Xây dựng mỗi phím bàn phím
        return NumberButton(
          number: index == 9 ? 'C' : '${index + 1}',
          onPressed: () {
            // Xử lý sự kiện nhấn nút
            if (index == 9) {
              // Xử lý nút xóa
            } else {
              // Xử lý nút số
              print('Pressed number ${index + 1}');
            }
          },
        );
      },
    );
  }
}

class NumberButton extends StatelessWidget {
  final String number;
  final VoidCallback onPressed;

  const NumberButton({
    Key? key,
    required this.number,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        padding: const EdgeInsets.all(16.0),
        backgroundColor: Colors.blue,
        textStyle: const TextStyle(fontSize: 20.0),
      ),
      child: Text(number),
    );
  }
}
