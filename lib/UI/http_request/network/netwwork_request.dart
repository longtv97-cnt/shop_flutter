﻿import 'dart:convert';

import 'package:shop/UI/http_request/model/Post.dart';
import 'package:http/http.dart' as http;

class NetworkRequest {
  static const String url = "https://jsonplaceholder.typicode.com/posts";
  static List<Post> parsePosts(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    List<Post> posts = parsed.map<Post>((json) => Post.fromJson(json)).toList();
    return posts;
  }

  static Future<List<Post>> fetchPosts({int page=1}) async {
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return parsePosts(response.body);
    } else if (response.statusCode == 404) {
      throw Exception("404");
    } else {
      throw Exception("Can't get posts");
    }
  }
}
