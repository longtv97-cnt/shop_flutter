﻿import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: MyWidget(),
  ));
}

class MyWidget extends StatefulWidget {
  const MyWidget({super.key});

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  // Khởi tạo PageController để điều khiển PageView
  PageController controller = PageController();
  int currentIndex = 0; // Biến lưu trữ chỉ số trang hiện tại

  // Hàm chuyển đổi trang
  void nextPage(int index) {
    setState(() {
      currentIndex = index; // Cập nhật chỉ số trang hiện tại
      controller.jumpToPage(index); // Chuyển đến trang tương ứng
    });
  }

  // Danh sách các màn hình
  final List<Widget> screens = <Widget>[
    Container(
      color: Colors.lime,
      child: Center(
        child: Text(
          "Screen 1",
          style: TextStyle(fontSize: 30, color: Colors.white),
        ),
      ),
    ),
    Container(
      color: Colors.blue,
      child: Center(
        child: Text(
          "Screen 2",
          style: TextStyle(fontSize: 30, color: Colors.white),
        ),
      ),
    ),
    Container(
      color: Colors.black,
      child: Center(
        child: Text(
          "Screen 3",
          style: TextStyle(fontSize: 30, color: Colors.white),
        ),
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Đặt FloatingActionButton ở giữa dưới cùng
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor:currentIndex==1? Colors.red
        :Colors.black,
        child: Icon(Icons.alarm),
        onPressed: () {
          nextPage(1); // Chuyển đến trang thứ hai khi ấn vào FAB
        },
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(), // Tạo notch cho FAB
        notchMargin: 8, // Khoảng cách giữa FAB và BottomAppBar
        color: Colors.blue,
        child: Row(
          children: [
            Expanded(
              child: IconButton(
                icon: Icon(Icons.home),
                color: currentIndex == 0 ? Colors.white : Colors.black, // Đổi màu biểu tượng khi trang hiện tại là trang đầu tiên
                onPressed: () {
                  nextPage(0); // Chuyển đến trang đầu tiên khi ấn vào biểu tượng home
                },
              ),
            ),
            Expanded(
              child: SizedBox(), // Khoảng trống cho notch
            ),
            Expanded(
              child: IconButton(
                icon: Icon(Icons.message),
                color: currentIndex == 2 ? Colors.white : Colors.black, // Đổi màu biểu tượng khi trang hiện tại là trang cuối cùng
                onPressed: () {
                  nextPage(2); // Chuyển đến trang cuối cùng khi ấn vào biểu tượng message
                },
              ),
            ),
          ],
        ),
      ),
      // Sử dụng PageView.builder để tạo các trang
      body: PageView.builder(
        controller: controller, // Đặt controller cho PageView
        itemCount: screens.length, // Số lượng trang
        onPageChanged: (index) {
          setState(() {
            currentIndex = index; // Cập nhật chỉ số trang khi trang thay đổi
          });
        },
        itemBuilder: (context, index) {
          return screens[index]; // Trả về trang tương ứng
        },
      ),
    );
  }
}
