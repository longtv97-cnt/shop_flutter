﻿import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight
  ]);
  runApp(const MaterialApp(
    home: UI1(),
    debugShowCheckedModeBanner: false,
  ));
}

class UI1 extends StatelessWidget {
  const UI1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          child: Row(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(30),
                ),
                width: 40,
                height: 40,
                margin: const EdgeInsets.all(10.0),
              ),
              const Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Title', style: TextStyle(fontSize: 20)),
                    Text('Content', style: TextStyle(fontSize: 16)),
                  ],
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.play_arrow, size: 30, color: Colors.green),
              ),
              // ElevatedButton.icon(
              //     onPressed: () {},
              //     icon: Icon(Icons.favorite),
              //     label: Text('Like'))
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.favorite, color: Colors.red),
                  ),
                  Text('100')
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
