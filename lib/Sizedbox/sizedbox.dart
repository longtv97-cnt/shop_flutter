﻿
import 'package:flutter/material.dart';
void main() {
  runApp(const MaterialApp(
    home: Sizedbox(),
  ));
}
class Sizedbox extends StatefulWidget {
  const Sizedbox({Key? key}) : super(key: key);

  @override
  State<Sizedbox> createState() => _SizedboxState();
}

class _SizedboxState extends State<Sizedbox> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: SizedBox(
            width: 200,
            height: 300,
            child: ElevatedButton(
              onPressed: () {
                print('Button Clicked');
              },
              child: Text('Click Me'),
            ),
          ),
        ),
      ),
    );
  }
}
