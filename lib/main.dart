import 'package:flutter/material.dart';
import 'package:shop/UI/signup_signin/Login.dart';
import 'package:shop/UI/signup_signin/signup.dart';

void main() {
  runApp( MaterialApp(
    initialRoute: 'login',
   routes:<String, WidgetBuilder>{
      'login' : (context) => Login(),
      'signup' : (context) => SignUp(),
   },
    debugShowCheckedModeBanner: false,
  ));
}

