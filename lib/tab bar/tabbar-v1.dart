﻿import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TabBar Example',
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: Text('TabBar Example'),
            bottom: TabBar(
              tabs: [
                Tab(
                  child: Column(
                    children: [
                      Text('Thiên nhiên', style: TextStyle(fontSize: 16)),
                      Text('Số lượng bài: 10', style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
                Tab(
                  child: Column(
                    children: [
                      Text('Khoa học', style: TextStyle(fontSize: 16)),
                      Text('Số lượng bài: 15', style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
                Tab(
                  child: Column(
                    children: [
                      Text('Âm nhạc', style: TextStyle(fontSize: 16)),
                      Text('Số lượng bài: 20', style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              ListTile(
  leading: Icon(Icons.account_circle), // Biểu tượng bên trái
  title: Text('John Doe'), // Tiêu đề
  subtitle: Text('Software Developer'), // Phụ đề
  trailing: Icon(Icons.arrow_forward), // Biểu tượng bên phải
  onTap: () {
    // Hành động khi ListTile được nhấn
  },
),
              Center(child: Text('Content of Tab 2')),
              Center(child: Text('Content of Tab 3')),
            ],
          ),
        ),
      ),
    );
  }
}
