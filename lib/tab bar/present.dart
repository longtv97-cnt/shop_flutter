﻿
import 'package:flutter/material.dart';

void main() {
  var title = ['All', 'Free', 'Reserved'];
  var amount = [15, 5, 10];

  runApp(
    MaterialApp(
      home: DefaultTabController(
        length: title.length,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Tab Bar Example'),
            bottom: TabBar(
              tabs: [
                for (int i = 0; i < title.length; i++)
                  Tab(
                    child: Column(
                      children: [
                        Text(title[i], style: TextStyle(fontSize: 16)),
                        Text('${amount[i]}', style: TextStyle(fontSize: 12)),
                      ],
                    ),
                  )
              ],
            ),
          ),
          body: TabBarView(
            children: [
              for (int i = 0; i < title.length; i++)
                Center(
                  child: Text('Content for ${title[i]}'),
                )
            ],
          ),
        ),
      ),
    ),
  );
}
