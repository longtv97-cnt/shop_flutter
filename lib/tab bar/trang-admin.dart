﻿import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: AdminApp(),
      debugShowCheckedModeBanner: false,
    ),
  );
}

class AdminApp extends StatefulWidget {
  @override
  _AdminAppState createState() => _AdminAppState();
}

class _AdminAppState extends State<AdminApp> {
  var titleTab = ['All', 'Free', 'Reserved'];
  var amount = [15, 5, 10];
  var pages = ['Tables', 'Items', 'Orders', 'Users', 'Stats'];
  var _currentIndex = 1;
  var _icon = [
    Icons.table_rows_rounded,
    Icons.icecream,
    Icons.shopping_cart,
    Icons.people,
    Icons.bar_chart
  ];
  var tables = ['4', '3', '9', '2', '11', '6', '11', '13', '15'];

  List<bool> isButtonPressedList = List.generate(9, (index) => false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                // Add your menu button onPressed logic here
              },
            ),
            const Text('Tables'),
            IconButton(
              icon: Stack(
                children: [
                  const Icon(Icons.notifications, size: 40),
                  Positioned(
                    right: 0,
                    child: Container(
                      constraints:
                          const BoxConstraints(minWidth: 20, minHeight: 20),
                      padding: const EdgeInsets.all(2),
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ],
              ),
              onPressed: () {
                // Add your notifications button onPressed logic here
              },
            ),
          ],
        ),
      ),
      body: DefaultTabController(
        length: titleTab.length,
        initialIndex: _currentIndex,
        child: Column(
          children: [
            TabBar(
              onTap: (index) => setState(() => _currentIndex = index),
              indicatorColor: Colors.red,
              tabs: [
                for (int i = 0; i < titleTab.length; i++)
                  Tab(
                    child: Column(
                      children: [
                        Text(titleTab[i], style: const TextStyle(fontSize: 16)),
                        Text('${amount[i]}',
                            style: const TextStyle(fontSize: 12)),
                      ],
                    ),
                  ),
              ],
            ),
            Expanded(
              child: TabBarView(
                children: [
                  for (int i = 0; i < titleTab.length; i++)
                    if (titleTab[i] == 'All')
                      Container(
                        child: const Center(
                          child: Text('All Tables'),
                        ),
                      )
                    else if (titleTab[i] == 'Free')
                      Container(
                        width: MediaQuery.of(context).size.width * 0.8,
                        height: MediaQuery.of(context).size.height * 0.4,
                        child: Column(
                          children: [
                            const Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    'Select Table: ',
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.blue),
                                  ),
                                ),
                              ],
                            ),
                            Expanded(
                              child: GridView.builder(
                                gridDelegate:
                                    const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 10,
                                  childAspectRatio: 1,
                                ),
                                itemCount: tables.length,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    splashColor: Colors.blue,
                                    onTap: () {
                                      setState(() {
                                        isButtonPressedList[index] =
                                            !isButtonPressedList[index];
                                      });
                                    },
                                    child: Container(
                                      color: isButtonPressedList[index]
                                          ? Colors.blue
                                          : Colors.transparent,
                                      child: Center(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              tables[index],
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  color:
                                                      isButtonPressedList[index]
                                                          ? Colors.white
                                                          : Colors.blueGrey),
                                            ),
                                            const Text(
                                              'Table',
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  color: Colors.blueGrey),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.all(10),
                              child: const Center(
                                child: Text(
                                  '120.25',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                                margin: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.all(5),
                                      color: Colors.redAccent,
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                                shape:
                                                    const RoundedRectangleBorder(),
                                                backgroundColor:
                                                    Colors.redAccent),
                                            onPressed: () {
                                              // Add your onPressed logic here
                                            },
                                            child: const Text(
                                              'Payment',
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                          Icon(
                                            Icons.payment,
                                            color: Colors.white,
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.all(5),
                                      color: Colors.redAccent,
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                                shape:
                                                    const RoundedRectangleBorder(),
                                                backgroundColor:
                                                    Colors.redAccent),
                                            onPressed: () {
                                              // Add your onPressed logic here
                                            },
                                            child: const Text(
                                              'View',
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                          Icon(Icons.search,
                                              color: Colors.white)
                                        ],
                                      ),
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      )
                    else if (titleTab[i] == 'Reserved')
                      Container(
                        child: const Center(
                          child: Text('Reserved Tables'),
                        ),
                      )
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: [
          for (int i = 0; i < pages.length; i++)
            BottomNavigationBarItem(
              icon: Icon(_icon[i]),
              label: pages[i],
            ),
        ],
      ),
    );
  }
}
