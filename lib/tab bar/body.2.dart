﻿import 'package:flutter/material.dart';

void main() {
  var title = ['All', 'Free', 'Reserved'];
  var amount = [15, 5, 10];

  var combinedList = zipLists(title, amount);

  runApp(
    MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Tab Bar Example'),
            bottom: TabBar(
              tabs: combinedList.map((item) => Tab(text: '${item[0]} (${item[1]})')).toList(),
            ),
          ),
          body: TabBarView(
            children: combinedList.map((item) => Center(child: Text('${item[0]}: ${item[1]}'))).toList(),
          ),
        ),
      ),
    ),
  );
}

List<List<dynamic>> zipLists(List<dynamic> list1, List<dynamic> list2) {
  return List.generate(
    list1.length,
    (index) => [list1[index], list2[index]],
  );
}
